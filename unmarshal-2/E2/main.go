package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type City struct {
	Precision string  `json:"precision,omitempty"`
	Latitude  float64 `json:"Latitude,omitempty"`
	Longitude float64 `json:"Longitude,omitempty"`
	Address   string  `json:"Address,omitempty"`
	City      string  `json:"City,omitempty"`
	State     string  `json:"State,omitempty"`
	Zip       string  `json:"Zip,omitempty"`
	Country   string  `json:"Country,omitempty"`
}

type cities []City

func main() {

	var data cities
	rcvd := `[{"precision":"zip","Latitude":37.7668,"Longitude":-122.3959,"Address":"","City":"SAN FRANCISCO","State":"CA","Zip":"94107","Country":"US"},
			  {"precision":"zip","Latitude":37.371991,"Longitude":-122.02602,"Address":"","City":"SUNNYVALE","State":"CA","Zip":"94085","Country":"US"}]`
	err := json.Unmarshal([]byte(rcvd), &data)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(data)

}
