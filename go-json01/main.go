package main

import (
	"encoding/json"
	"fmt"
	"os"
)

func main() {

	type ColorGroup struct {
		ID     int
		Name   string
		Colors []string
	}

	gc := ColorGroup{
		ID:     1,
		Name:   "Reds",
		Colors: []string{"Crimson", "Red", "Runby", "Maroon"},
	}

	encoder := json.NewEncoder(os.Stdin)

	encoder.SetIndent("", "    ")
	err := encoder.Encode(gc)

	if err != nil {
		fmt.Println(err)
	}

}
