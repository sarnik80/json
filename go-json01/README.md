# GO To JSON 01

# Marshal and Encode

- the most important thing to understand is you can _Marshal_ **OR** _Encode_ go code to json .

- bedon tavajoh be inke az kodom estefade konim toye hardo halat **GO DATA STRUCTURE** will be turned into **JSON**

- **So what's the difference?**

* Marshal is for turning Go data structure into JSON and then assigning JSON to the variable

* encode is used to turn GO datastructures into JSON and then send it over the wire .

- [encoding/json][json package]

[json package]: https://pkg.go.dev/encoding/json

- baraye inke az encode function estefade konim ye pointer be yek Encoder mikhahim .

  ![encoder][encoder]

  [encoder]: https://gitlab.com/sarnik80/json/uploads/7d96bf835821b9e81a55eac2e26b8870/Screenshot_from_2023-08-07_10-17-23.png

- NewEncoder ye io.writer migire yani man daram encode mikonam toye in stream

  ```go
          func NewEncoder(w io.Writer) *Encoder

          func (enc *Encoder) Encode (v eny) error
  ```

* vaghti yek pointer be yek Encider dashte bashim be har 3 function dige dastresi darim .

- hamchenin midonim k responseWriter implement mikone writer interface ro

- Example for using Encode in golang :

  ```go
    package main

    import (
        "encoding/json"
        "fmt"
        "os"
    )

    func main() {

        type ColorGroup struct {
            ID     int
            Name   string
            Colors []string
        }

        gc := ColorGroup{
            ID:     1,
            Name:   "Reds",
            Colors: []string{"Crimson", "Red", "Runby", "Maroon"},
        }

        encoder := json.NewEncoder(os.Stdin)

        encoder.SetIndent("", "    ")
        err := encoder.Encode(gc)

        if err != nil {
            fmt.Println(err)
        }

    }

  ```

  - ma inja darim migim toye os.Stdin encode kon va ghablesh behesh indent ham midim

- Example for using Marshal in golang :

  ```go

          package main

          import (
              "encoding/json"
              "fmt"
              "os"
          )

          func main() {

              type ColorGroup struct {
                  ID     int
                  Name   string
                  Colors []string
              }

              gc := ColorGroup{
                  ID:     1,
                  Name:   "Reds",
                  Colors: []string{"Crimson", "Red", "Runby", "Maroon"},
              }

              b, err := json.MarshalIndent(gc, "", "    ")

              if err != nil {
                  fmt.Println(err)
              }

              fmt.Printf("%T\n", b)
              os.Stdout.Write(b)

          }

  ```

  - output :

  ```bash
      []uint8

      {
          "ID": 1,
          "Name": "Reds",
          "Colors": [
              "Crimson",
              "Red",
              "Runby",
              "Maroon"
          ]
      }%
  ```

- be unmarshal function data e k midim bayad []byte bashe va toye ye pointer be on value store mikone go code ro

- ```go
  func Unmarshal(data []byte, v eny) error
  ```

- Unmarshal parses the JSON-encoded data and stores the result in the value pointed to by v .
