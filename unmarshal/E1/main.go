package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type thumbnail struct {
	URL    string
	Height int
	Width  int
}

type Image struct {
	Width     int
	Height    int
	Title     string
	Thumbnail thumbnail
	Animated  bool
	IDs       []int
}

func main() {

	http.HandleFunc("/", resp)
	http.ListenAndServe(":8080", nil)

}

func resp(w http.ResponseWriter, r *http.Request) {
	rcvd := `{"Width":800,"Height":600,"Title":"View from 15th Floor","Thumbnail":{"Url":"http://www.example.com/image/481989943","Height":125,"Width":100},"Animated":false,"IDs":[116,943,234,38793]}`

	var image Image

	err := json.Unmarshal([]byte(rcvd), &image)

	if err != nil {
		log.Fatalln(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	fmt.Fprint(w, rcvd)
}
