package main

import (
	"encoding/json"
	"fmt"
)

type City struct {
	Postal    string  `json:"Postal"`
	Latitude  float64 `json:"Latitude"`
	Longitude float64 `json:"Longitude"`
	Address   string  `json:"Address"`
	City      string  `json:"City"`
	State     string  `json:"State"`
	Zip       string  `json:"Zip"`
	Country   string  `json:"Country"`
}

type cities []City

func main() {

	// this is a JSON data that we received
	rcv := `[
        {
            "Postal": "zip",
            "Latitude":  37.7668,
            "Longitude": -122.3959,
            "Address":   "",
            "City":      "SAN FRANCISCO",
            "State":     "CA",
            "Zip":       "94107",
            "Country":   "US"
        },
        {
            "Postal": "zip",
            "Latitude":  37.371991,
            "Longitude": -122.026020,
            "Address":   "",
            "City":      "SUNNYVALE",
            "State":     "CA",
            "Zip":       "94085",
            "Country":   "US"
        }
    ]`

	var c cities

	err := json.Unmarshal([]byte(rcv), &c)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(c)
}
