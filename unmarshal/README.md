# Unmarshal JSON with GO

## Notes

- ```go
  func Unmarshal(data []byte, v eny) error
  ```

- Unmarshal parses the JSON-encoded data and stores the result in the value pointed to by v .

- [documwntation](https://pkg.go.dev/encoding/json#Unmarshal)
