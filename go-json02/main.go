package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type person struct {
	Fname string   `json:"fname"`
	Lname string   `json:"lname"`
	Items []string `json:"items"`
}

func main() {

	http.HandleFunc("/", foo)
	http.HandleFunc("/mshl", mshl)
	http.HandleFunc("/encd", encd)
	http.ListenAndServe(":8080", nil)

}

func foo(w http.ResponseWriter, r *http.Request) {

	s := `<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta charset="UTF-8">
	<title>FOO</title>
	</head>
	<body>
	You are at foo
	</body>
	</html>`
	w.Write([]byte(s))
}

func mshl(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	p1 := person{
		Fname: "James",
		Lname: "Bond",
		Items: []string{"1", "2", "3", "5"},
	}

	jb, err := json.Marshal(p1)

	if err != nil {
		fmt.Println(err)
	}

	w.Write(jb)

}

func encd(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	p1 := person{
		Fname: "James",
		Lname: "Bond",
		Items: []string{"Suit", "Gun", "Wry sense of humor"},
	}

	// data ro encode mikonim toye responsewriter mon

	err := json.NewEncoder(w).Encode(p1)

	if err != nil {
		fmt.Println(err)
	}

}
