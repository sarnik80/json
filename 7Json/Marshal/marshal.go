package example

import (
	"encoding/json"
	"fmt"
)

type Person struct {
	Name   string `json:"name"`
	Family string `json:"fname,omitempty"`
	Age    int    `json:"-"`
}

type people []Person

func Example() {
	person_1 := Person{
		Name:   "sara",
		Family: "nik",
		Age:    21,
	}

	person_2 := Person{
		Name:   "sim",
		Family: "",
		Age:    21,
	}

	var p people

	p = append(p, person_1, person_2)

	json_p1b, err := json.MarshalIndent(p, "", "\t")

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(json_p1b))

}

func UnmarshalExample() {
	jsondata := []byte(`{"name": "sara","fname": "nik"}`)

	p := Person{}

	err := json.Unmarshal(jsondata, &p)

	if err != nil {
		fmt.Println(err)

	}

	fmt.Println(p)

}
