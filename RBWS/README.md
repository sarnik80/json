# Simple Rest-based Web Service

## HandlePost

```go
    func handlePost(w http.ResponseWriter, r *http.Request) (err error) {

        len := r.ContentLength

        body := make([]byte, len) // creates slice of bytes

        r.Body.Read(body)  // Reads request body into slice

        var post Post

        json.Unmarshal(body, &post)  // Unmarshals byte slice into Post struct

        err = post.create()  // Creates database record

        if err != nil {
            return
        }

       	w.WriteHeader(http.StatusOK)

        return
    }

```

1. First you create a slice of bytes with the correct content length size and read the contents of the body.

> **Note tht :** content of body is a JSON string .

2. Next, you declare a Post struct and unmarshal the content into it.

> Use cURL to call the web service :

- ```bash
    curl -i -X POST -H "Content-Type: application/json" -d '{"content": "first post from curl", "author":"curl"}'  http://127.0.0.1:8080/post/

  ```

- you should see something like this :

- ```bash
    HTTP/1.1 200 OK
    Vary: Origin
    Date: Thu, 17 Aug 2023 05:15:00 GMT
    Content-Length: 0

  ```

##

## HandleGet

> Function that retrieve a post

- ```go
                 // handleGet retrieves post
           func (app *Config) handleGet(w http.ResponseWriter, r *http.Request) {

             id, err := strconv.Atoi(chi.URLParam(r, "id"))

             if err != nil {
               return
             }

             post, err := retrieve(id)  // Gets data from database into Post struct

             if err != nil {
               return
             }

             byteData, err := json.MarshalIndent(post, "", "\t") // Marshals the Post struct into JSON string

             if err != nil {
               return
             }

             w.Header().Set("Content-Type", "application/json")  // Writes JSO  to ResponseWriter

             w.Write(byteData)

             return
           }

  ```

  > from [**chi**](https://go-chi.io/#/pages/routing?id=routing-patterns-amp-url-parameters) documentation

  - each routhing method accepts a URL _pattern_ and chain of _handlers_

  * the URL pattern supports named params -> e.g : /users/{userID} or /posts/{postID}

  - URL parameters can be fetched at runtime by calling chi.URLParam(r, "id") for named parameters

  - ```go
       apiRouter.Get("/{id}", app.handleGet)
    ```
  - the url parameters are defined using the curly brackets {} with the parameter name in between

  - When a HTTP request is sent to the server and handled by the chi router, if the URL path matches the format of /post/{id} , then the handleGet function will be called to send a response to the client.

> To see how this works , run this command on the terminal:

```bash
  curl -i -X GET http://127.0.0.1:8080/post/1
```

output :

```bash

  HTTP/1.1 200 OK
  Content-Type: application/json
  Vary: Origin
  Date: Thu, 17 Aug 2023 06:09:54 GMT
  Content-Length: 67

  {
    "id": 1,
    "content": "first post from curl",
    "author": "curl"
  }
```

##

## SUMMARY

- REST-based web services expose resources over HTTP and allow specific
  actions on them
