package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
)

// handleGet retrieves post
func (app *Config) handleGet(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(chi.URLParam(r, "id"))

	if err != nil {
		return
	}

	post, err := retrieve(id)

	if err != nil {
		return
	}

	byteData, err := json.MarshalIndent(post, "", "\t")

	if err != nil {
		return
	}

	w.Header().Set("Content-Type", "application/json")

	w.Write(byteData)

	return
}

// hello to everyone
func (app *Config) hello(w http.ResponseWriter, r *http.Request) {

	msg := struct {
		Data string `json:"message"`
	}{

		Data: "Hello",
	}

	json.NewEncoder(w).Encode(msg)

	return
}

// handlePost create a post
func (app *Config) handlerPost(w http.ResponseWriter, r *http.Request) {

	len := r.ContentLength

	body := make([]byte, len)

	r.Body.Read(body)

	var post Post

	err := json.Unmarshal(body, &post)

	if err != nil {
		fmt.Println(err)
		return
	}

	err = post.create()

	if err != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	return

}

// handlePut updates post
func (app *Config) handlePut(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(chi.URLParam(r, "id"))

	if err != nil {
		return
	}

	post, err := retrieve(id)
	if err != nil {
		return
	}

	len := r.ContentLength

	body := make([]byte, len)

	r.Body.Read(body)

	err = json.Unmarshal(body, &post)

	if err != nil {
		return
	}

	err = post.update()

	w.WriteHeader(http.StatusOK)
	return
}

// handleDelete deletes a post
func (app *Config) handleDelete(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(chi.URLParam(r, "id"))

	if err != nil {
		return
	}

	post, err := retrieve(id)
	if err != nil {
		return
	}

	err = post.delete()

	if err != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}
