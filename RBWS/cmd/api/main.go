package main

import (
	"database/sql"
	"fmt"
	"net/http"

	_ "github.com/lib/pq"
)

// Post is our model
type Post struct {
	ID      int    `json:"id"`
	Content string `json:"content"`
	Author  string `json:"author"`
}

var (
	db      *sql.DB
	webPort = "8080"
)

// connects to the DB
func init() {

	var err error

	db, err = sql.Open("postgres", "host=db  port=5432 user=postgres dbname=tmp password=2980 sslmode=disable")

	if err != nil {
		panic(err)
	}

}

//Config is a receiver for our application
type Config struct{}

func main() {

	app := Config{}

	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", webPort),
		Handler: app.routes(),
	}

	err := server.ListenAndServe()

	if err != nil {
		fmt.Println(err)
	}

}
