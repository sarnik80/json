package main

// retrieve Gets a single post
func retrieve(id int) (post Post, err error) {

	post = Post{}

	err = db.QueryRow("select * from posts where id = $1", id).Scan(&post.ID, &post.Content, &post.Author)

	return

}

//create creates a new post
func (post *Post) create() (err error) {

	statement := "insert into posts (content , author) values ($1 , $2) returning id"

	stmt, err := db.Prepare(statement)

	if err != nil {
		return
	}

	defer stmt.Close()

	err = stmt.QueryRow(&post.Content, &post.Author).Scan(&post.ID)

	return

}

//update updates a post
func (post *Post) update() (err error) {

	_, err = db.Exec("update posts set content=$1 , author=$2 where id = $3", post.Content, post.Author, post.ID)

	return
}

// delete deletes a post
func (post *Post) delete() (err error) {

	_, err = db.Exec("delete from posts where id = $1", post.ID)

	return
}
