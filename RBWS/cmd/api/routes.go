package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
)

// routes is a function to multiplex request to the correct function
func (app *Config) routes() http.Handler {

	mux := chi.NewRouter()

	// specify who is allowed to connect
	mux.Use(cors.Handler(cors.Options{

		// Use this to allow specific origin hosts
		// AllowedOrigins:   []string{"https://foo.com"},
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	}))

	mux.Use(middleware.Heartbeat("/ping"))
	mux.Get("/", app.hello)

	apiRouter := chi.NewRouter()

	apiRouter.Get("/{id}", app.handleGet)
	apiRouter.Post("/", app.handlerPost)
	apiRouter.Put("/{id}", app.handlePut)
	apiRouter.Delete("/{id}", app.handleDelete)

	mux.Mount("/post", apiRouter)

	return mux

}
