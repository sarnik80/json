FROM golang:1.18-alpine AS builder


RUN mkdir app

WORKDIR /app


ENV CGO_ENABLED 0
ENV GOPATH /go
ENV GOCACHE /go-build


COPY go.mod go.sum ./
RUN --mount=type=cache,target=/go/pkg/mod/cache \
    go mod download

COPY . . 

RUN --mount=type=cache,target=/go/pkg/mod/cache \
    --mount=type=cache,target=/go-build \
    go build -o target ./api



RUN chmod +x ./target

FROM alpine:3.18.2

RUN mkdir app

WORKDIR /app


COPY --from=builder /app/target /app


EXPOSE 8080

CMD [ "./target" ]